package config;


public class Configuration {

	private static Configuration instance;

	private volatile int noOfQueues;
	private volatile int simulationInterval;
	private volatile int minArrivingTime;
	private volatile int maxArrivingTime;
	private volatile int minServiceTime;
	private volatile int maxServiceTime;
	private volatile int numberOfGeneratedClients;

	private Configuration() {
		this.noOfQueues = 4;
		this.simulationInterval = 30;
		this.minArrivingTime = 1;
		this.minServiceTime = 2;
		this.maxServiceTime = 6;
		this.maxArrivingTime = simulationInterval - maxServiceTime - 1;
		this.numberOfGeneratedClients = 15;
	}
	public static Configuration getInstance(){
		if(instance==null){
			instance = new Configuration();
		}
		return instance;
	}
	public int getNoOfQueues() {
		return noOfQueues;
	}
	public void setNoOfQueues(int noOfQueues) {
		this.noOfQueues = noOfQueues;
	}
	public int getSimulationInterval() {
		return simulationInterval;
	}
	public void setSimulationInterval(int simulationInterval) {
		this.simulationInterval = simulationInterval;
	}
	public int getMinArrivingTime() {
		return minArrivingTime;
	}
	public void setMinArrivingTime(int minArrivingTime) {
		this.minArrivingTime = minArrivingTime;
	}
	public int getMaxArrivingTime() {
		return maxArrivingTime;
	}
	public void setMaxArrivingTime(int maxArrivingTime) {
		this.maxArrivingTime = maxArrivingTime;
	}
	public int getMinServiceTime() {
		return minServiceTime;
	}
	public void setMinServiceTime(int minServiceTime) {
		this.minServiceTime = minServiceTime;
	}
	public int getMaxServiceTime() {
		return maxServiceTime;
	}
	public void setMaxServiceTime(int maxServiceTime) {
		this.maxServiceTime = maxServiceTime;
	}
	public int getNumberOfGeneratedClients() {
		return numberOfGeneratedClients;
	}
	public void setNumberOfGeneratedClients(int numberOfGeneratedClients) {
		this.numberOfGeneratedClients = numberOfGeneratedClients;
	}
}
