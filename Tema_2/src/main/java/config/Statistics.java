package config;

public class Statistics {

	private static Statistics instance;
	
	private int numberOfClients;
	private int totalWaitTime;
	private int emptyQueueTime;
	
	private Statistics() {
		this.numberOfClients = 0;
		this.totalWaitTime = 0;
		this.emptyQueueTime = 0;
	}
	
	public static Statistics getInstance() {
		if (instance == null) {
			instance = new Statistics();
		}
		
		return instance;
	}
	
	public void addWaitTimeForAClient(int waitTime) {
		if (waitTime > 0 ){
			this.numberOfClients++;
			totalWaitTime+=waitTime;
		}
	}
	
	public void resetStats() {
		instance = new Statistics();
	}
	
	public void incrementEmptyQueueTime() {
		this.emptyQueueTime++;
	}
	
	public double getAverageWaitTime() {
		return ((double) totalWaitTime) / numberOfClients;
	}
	public int getNoClients() {
		return numberOfClients;
	}
	
	public int getEmptyQueue() {
		return emptyQueueTime;
	}
}
