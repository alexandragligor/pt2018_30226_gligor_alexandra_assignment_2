package queues;

import java.util.LinkedList;
import java.util.Queue;

import config.Statistics;
import gui.GUI;
import simulator.SimulationControl;

public class ShopQueue extends Thread {
	private Queue<Client> clients;
	private int currentWaitTime;

	public ShopQueue() {
		clients = new LinkedList<>();
		this.currentWaitTime = 0;
	}

	@Override
	public void run() {
		
		GUI.getInstance().addQueue(Thread.currentThread().getId());
		
		while (SimulationControl.doRun) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Client c;
			c = clients.peek();
			
			if (c != null) {
				System.out.println("Am client bro");
				if (c.getServiceTime() > 0) {
					c.decServiceTime();
				}
				if (c.getServiceTime() == 0) {
					clients.remove();
				}
			} else {
				Statistics.getInstance().incrementEmptyQueueTime();
			}
			
			if (currentWaitTime > 0) {
				currentWaitTime--;
			}
			
			GUI.getInstance().updateQueue(Thread.currentThread().getId(), currentWaitTime, clients.size());
			
			System.out.println("I'm thread " + Thread.currentThread().getId() + "--" + SimulationControl.doRun);
		}
	}
	
	public int getCurrentWaitTime() {
		return this.currentWaitTime;
	}
	
	public void addClient(Client c) {
		Statistics stats = Statistics.getInstance();
		stats.addWaitTimeForAClient(currentWaitTime);
		this.currentWaitTime += c.getServiceTime();
		clients.add(c);
	}
}
