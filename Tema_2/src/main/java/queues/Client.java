package queues;

public class Client {
	private int arrivingTime;
	private int serviceTime;

	public Client(int arrivingTime, int serviceTime) {
		this.setArrivingTime(arrivingTime);
		this.setServiceTime(serviceTime);
	}

	public int getArrivingTime() {
		return arrivingTime;
	}

	public void setArrivingTime(int arrivingTime) {
		this.arrivingTime = arrivingTime;
	}

	public int getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}
	public void decServiceTime(){
		serviceTime--;
	}
	
}
