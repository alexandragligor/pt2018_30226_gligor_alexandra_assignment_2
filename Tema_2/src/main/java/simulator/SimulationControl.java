package simulator;

import java.util.List;
import java.util.Map;

import config.Configuration;
import config.Statistics;
import gui.GUI;
import queues.Client;
import queues.ShopQueue;

public class SimulationControl implements Runnable {
	
	private int simulationTimer;
	private int passedSeconds;
	
	private List<ShopQueue> runningThreads;
	private Map<Integer, List<Client>> clientMap;
	
	public static volatile boolean doRun;
	
	public SimulationControl(List<ShopQueue> runningThreads, Map<Integer, List<Client>> clientMap) {
		simulationTimer = Configuration.getInstance().getSimulationInterval();
		this.runningThreads = runningThreads;
		this.clientMap = clientMap;
		this.passedSeconds = 0;
	}
	
	public void run() {
		while (doRun) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			passedSeconds++;
			
			List<Client> clientsArrived = clientMap.get(passedSeconds);
			
			if (clientsArrived != null) {
				clientsArrived.forEach(client -> {
					
					int minimumWaitTime = Integer.MAX_VALUE;
					ShopQueue bestCandidate = null;
					
					System.out.println("RUNNING THREADS SIZE" + runningThreads.size());
					
					for (ShopQueue thread : runningThreads) {
						int waitTime = thread.getCurrentWaitTime();
						System.out.println("CurrentWAiTYEJSLKFD" + waitTime);
						if (waitTime < minimumWaitTime) {
							minimumWaitTime = waitTime;
							bestCandidate = thread;
						}
					}
					
					if (bestCandidate != null) {
						bestCandidate.addClient(client);
					} else {
						throw new IllegalStateException("Asa ceva nu se poate.");
					}
				});
			}
			
			System.out.println(String.format("Control thread time left: %s ", simulationTimer));
			
			if (simulationTimer > 0) {
				simulationTimer --;
			}
			
			if (simulationTimer == 0) {
				System.out.println("Yup ITS OVER BROTHEEER LOLOLOLOL");
				Statistics stat = Statistics.getInstance();
				GUI.getInstance().noClients.setText(Integer.toString(stat.getNoClients()));
				GUI.getInstance().totalWaitTime.setText(Double.toString(stat.getAverageWaitTime()));
				GUI.getInstance().emptyQueueTime.setText(Integer.toString(stat.getEmptyQueue()));
				doRun = false;
			}
		}
	}

}
