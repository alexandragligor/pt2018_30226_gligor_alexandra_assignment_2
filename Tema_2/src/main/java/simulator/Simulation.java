package simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import config.Configuration;
import config.Statistics;
import gui.GUI;
import queues.Client;
import queues.ShopQueue;

public class Simulation {
	
	public void startSimulation() {
		
		int numberOfQueues = Configuration.getInstance().getNoOfQueues();
		SimulationControl.doRun = true;
		
		Map<Integer, List<Client>> clientMap = generateClients();
		
		List<ShopQueue> runningThreads = new ArrayList<>();
		
        for (int i = 0; i < numberOfQueues; i++) {
            ShopQueue shopQueue = new ShopQueue();
            runningThreads.add(shopQueue);
            shopQueue.start();
        }
        
        Thread simulationControl = new Thread(new SimulationControl(runningThreads, clientMap));
        simulationControl.start();
        
        while (SimulationControl.doRun) {
        	// Wait for the simulation to finish
        }
        
        Statistics statistics = Statistics.getInstance();
        
        System.out.println("============================");
        System.out.println(String.format("Average wait time: %s", statistics.getAverageWaitTime()));
        
        System.out.println("Finished simulation");
	}
	
	private Map<Integer, List<Client>> generateClients() {
		Map<Integer, List<Client>> clientMap = new HashMap<>();
		Configuration config = Configuration.getInstance();

		for (int i = 0; i<config.getNumberOfGeneratedClients(); i++) {
			Client client = new Client(ThreadLocalRandom.current().nextInt(config.getMinArrivingTime(), config.getMaxArrivingTime() + 1),
					ThreadLocalRandom.current().nextInt(config.getMinServiceTime(), config.getMaxServiceTime() + 1));
			if (clientMap.get(client.getArrivingTime()) != null) {
				clientMap.get(client.getArrivingTime()).add(client);
			} else {
				List<Client> clients = new ArrayList<>();
				clients.add(client);
				clientMap.put(client.getArrivingTime(), clients);
			}
		}
		
//		Client a = new Client(2, 4);
//		Client b = new Client(3, 4);
//		Client c = new Client(4, 4);
//		Client d = new Client(4, 4);
//		
//		clientMap.put(2, Arrays.asList(a));
//		clientMap.put(3, Arrays.asList(b));
//		clientMap.put(4, Arrays.asList(c, d));
		
		return clientMap;
	}
	
	public static void main(String[] args){
		GUI.getInstance();
		new Simulation().startSimulation();
	}
}
