package gui;

import javax.swing.JTextArea;

public class GUIQueue {
	private long queueId;
	private JTextArea waitTimeArea;
	private JTextArea clientCountArea;
	
	
	public JTextArea getWaitTimeArea() {
		return waitTimeArea;
	}
	public void setWaitTimeArea(JTextArea waitTimeArea) {
		this.waitTimeArea = waitTimeArea;
	}
	public JTextArea getClientCountArea() {
		return clientCountArea;
	}
	public void setClientCountArea(JTextArea clientCountArea) {
		this.clientCountArea = clientCountArea;
	}
	public long getQueueId() {
		return queueId;
	}
	public void setQueueId(long queueId) {
		this.queueId = queueId;
	}
}
