package gui;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class GUI {
	
	private static GUI interfaceInstance;

	private static final int WINDOW_WIDTH = 800;
	private static final int SIMULATION_PANEL_HEIGHT = 200;
	
	private JFrame window;
	private JPanel queuePanel;
	private JPanel simulationPanel;
	private JPanel statPanel;
	
	
	private Map<Long, GUIQueue> guiQueues;
	public JLabel noClients;
	public JLabel totalWaitTime;
	public JLabel emptyQueueTime;
	
	private int queuePanelHeight;
	
	public static GUI getInstance() {
		if (interfaceInstance == null) {
			interfaceInstance = new GUI();
		}
		return interfaceInstance;
	}
	
	private GUI() {
		
		guiQueues = new HashMap<>();
		
		window = new JFrame("Simulation");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setSize(600, 430);
		window.setResizable(true);
		window.setLayout(null);
		
		queuePanelHeight = 0;
		
		queuePanel = new JPanel();
		queuePanel.setBackground(Color.GREEN);
		queuePanel.setLayout(null);
		queuePanel.setBounds(0, SIMULATION_PANEL_HEIGHT, WINDOW_WIDTH, queuePanelHeight);
		
		statPanel = new JPanel();
		statPanel.setLayout(null);
		statPanel.setBackground(Color.RED);
		statPanel.setBounds(0, SIMULATION_PANEL_HEIGHT + queuePanelHeight, WINDOW_WIDTH, 200);
		
		JLabel lNoClients = new JLabel("Clients No:");
		lNoClients.setBounds(10, 20, 60, 20);
		statPanel.add(lNoClients);
		
		noClients = new JLabel();
		noClients.setBounds(70, 20, 50, 20);
		statPanel.add(noClients);
		
		JLabel lWaitTime = new JLabel("Total waiting time:");
		lWaitTime.setBounds(10, 50, 130, 20);
		statPanel.add(lWaitTime);
		
		totalWaitTime = new JLabel();
		totalWaitTime.setBounds(140, 50, 50, 20);
		statPanel.add(totalWaitTime);
		
		JLabel lEmpty = new JLabel("Empty queue time:");
		lEmpty.setBounds(10, 80, 130, 20);
		statPanel.add(lEmpty);
		
		emptyQueueTime = new JLabel();
		emptyQueueTime.setBounds(140, 80, 50, 20);
		statPanel.add(emptyQueueTime);
	
		window.add(queuePanel);
		window.add(statPanel);
		window.setVisible(true);
	}
	
	public synchronized void addQueue(long queueId) {

		JLabel lWait = new JLabel("Waiting time at queue " +queueId+ ":");
		lWait.setBounds(10, queuePanelHeight + 10 , 150, 20);
		queuePanel.add(lWait);
		
		JTextArea waitTimeArea = new JTextArea();
		waitTimeArea.setText("0");
		waitTimeArea.setEditable(false);
		waitTimeArea.setBounds(160, queuePanelHeight + 10, 100, 40);
		
		JLabel lclient = new JLabel("Clients number at queue " +queueId+ ":");
		lclient.setBounds(270, queuePanelHeight + 10 , 160, 20);
		queuePanel.add(lclient);
		
		JTextArea clientCountArea = new JTextArea();
		clientCountArea.setText("0");
		clientCountArea.setEditable(false);
		clientCountArea.setBounds(440, queuePanelHeight + 10, 100, 40);
		queuePanelHeight += 60;
		
		GUIQueue queue = new GUIQueue();
		queue.setQueueId(queueId);
		queue.setWaitTimeArea(waitTimeArea);
		queue.setClientCountArea(clientCountArea);
		guiQueues.put(queueId, queue);
		
		queuePanel.add(waitTimeArea);
		queuePanel.add(clientCountArea);
		queuePanel.setBounds(0, 0, WINDOW_WIDTH, queuePanelHeight);
		statPanel.setBounds(0, queuePanelHeight, WINDOW_WIDTH, 200);
		
		window.revalidate();
		window.repaint();
	}
	
	public synchronized void updateQueue(long queueId, int waitTime, int clients) {
		GUIQueue queue = guiQueues.get(queueId);
		System.out.println("QID" + queueId);
		guiQueues.forEach((key, value) -> System.out.println(key + "-7-" + value.getQueueId()));
		queue.getClientCountArea().setText(String.valueOf(clients));
		queue.getWaitTimeArea().setText(String.valueOf(waitTime));
	}
}
